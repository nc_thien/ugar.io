using UnityEngine;
using System.Collections;

public class CellNameDisplay : MonoBehaviour
{
	[HideInInspector]
	public Cell cell;

	public TextMesh nameMesh;
		
	public void SetName(string name)
	{
		nameMesh.text = name;
	}
	
	public void Update()	
	{
		float fontSize = cell.GetCurrentSize() * 0.3f;
		nameMesh.transform.localScale = Vector3.one * fontSize;
	}
}

