﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Lidgren.Network;

public class Game : MonoBehaviour
{
	[System.Serializable]
	public class SkinInfo
	{
		public string name;

		[HideInInspector]
		public Texture texture;

		public bool isTransparent;
		public bool hasName;
	}

	public SkinInfo[] skins;
	private Dictionary<string, SkinInfo> skinMap;

	public GameObject cellPrefab;
	public GameObject linePrefab;

	private List<KeyValuePair<Transform, float>> lineCache;
	private int numLines;
	private int maxLines;

	public CameraScript cameraScript;

	private NetClient client;

	private Dictionary<int, Cell> cells;
	private Queue<Cell> cellCache;

	private Dictionary<GameMessageTypes, GameMessage> gameMessagePool;

	private bool isConnected;

	private float borderLeft;
	private float borderRight;

	private float borderTop;
	private float borderBottom;

	private int serverViewBaseX;
	private int serverViewBaseY;

	private float sendTargetTimer;

	[HideInInspector]
	public float serverTime;

	[HideInInspector]
	public LinearBin<CellRenderer.PointInfo> linearBin;

	private float linearBinMinX;
	private float linearBinMaxX;
	private float linearBinMinY;
	private float linearBinMaxY;

	public HUD hud;

	void Awake()
	{
		NetPeerConfiguration config = new NetPeerConfiguration("Ugar.io");
		config.EnableMessageType(NetIncomingMessageType.WarningMessage);
		config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
		config.EnableMessageType(NetIncomingMessageType.ErrorMessage);
		config.EnableMessageType(NetIncomingMessageType.Error);
		config.EnableMessageType(NetIncomingMessageType.DebugMessage);
		config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);
		
		config.MaximumHandshakeAttempts = 10;

		client = new NetClient(config);

		gameMessagePool = new Dictionary<GameMessageTypes, GameMessage>();
		gameMessagePool.Add(GameMessageTypes.SetTarget, new SetTargetMessage());
		gameMessagePool.Add(GameMessageTypes.SplitCell, new SplitCellMessage());
		gameMessagePool.Add(GameMessageTypes.EjectMass, new EjectMassMessage());
		gameMessagePool.Add(GameMessageTypes.AddCell, new AddCellMessage());
		gameMessagePool.Add(GameMessageTypes.UpdateCell, new UpdateCellMessage());
		gameMessagePool.Add(GameMessageTypes.HideCell, new HideCellMessage());
		gameMessagePool.Add(GameMessageTypes.DestroyCell, new DestroyCellMessage());
		gameMessagePool.Add(GameMessageTypes.UpdateCamera, new UpdateCameraMessage());
		gameMessagePool.Add(GameMessageTypes.GameLose, new GameLoseMessage());
		gameMessagePool.Add(GameMessageTypes.RespawnPlayer, new RespawnPlayerMessage());

		cameraScript.game = this;
		hud.game = this;

		skinMap = new Dictionary<string, SkinInfo>();

		int numSkins = skins.Length;
		for (int i = 0; i < numSkins; ++i)
		{
			skinMap.Add(skins[i].name, skins[i]);
		}

		cells = new Dictionary<int, Cell>();
		cellCache = new Queue<Cell>();

		lineCache = new List<KeyValuePair<Transform, float>>();
		numLines = maxLines = 0;

		linearBin = new LinearBin<CellRenderer.PointInfo>();

		isConnected = false;
		sendTargetTimer = 0.0f;
		serverTime = 0.0f;
	}

	public bool IsInBound(float x, float y, float offset = 0.0f)
	{
		if (x < borderLeft - offset || x > borderRight + offset || y < borderTop - offset || y > borderBottom + offset) return false;
		return true;
	}

	public GameMessage GetGameMessage(GameMessageTypes gameMessageType)
	{
		return gameMessagePool[gameMessageType];
	}

	private Transform GetLine(float time)
	{
		if (numLines < maxLines)
		{
			Transform res = lineCache[numLines++].Key;
			res.gameObject.SetActive(true);

			lineCache[numLines - 1] = new KeyValuePair<Transform, float>(res, time);

			return res;
		}
		else 
		{
			GameObject gameObject = GameObject.Instantiate(linePrefab) as GameObject;
			Transform res = gameObject.transform;
			lineCache.Add(new KeyValuePair<Transform, float>(res, time));

			numLines++;
			maxLines++;

			return res;
		}
	}

	public SkinInfo GetSkinInfo(string name)
	{
		if (string.IsNullOrEmpty(name)) return null;

		string lowerCaseName = name.ToLower();

		SkinInfo res;
		if (!skinMap.TryGetValue(lowerCaseName, out res)) return null;

		if (res.texture == null)
		{
			res.texture = Resources.Load<Texture2D>("Skins/" + lowerCaseName);
		}

		return res;
	}

	private void HideLines(int from, int to)
	{
		for (int i = from; i < to; ++i) lineCache[i].Key.gameObject.SetActive(false);
	}

	private Cell TryGetCell(int id)
	{
		Cell cell;
		if (cells.TryGetValue(id, out cell)) return cell;
		else return null;
	}

	private Cell GetCell(int id, CellType cellType)
	{
		Cell cell;
		if (cells.TryGetValue(id, out cell)) return cell;

		if (cellCache.Count > 0) 
		{
			cell = cellCache.Dequeue();
		}
		else 
		{
			GameObject gameObject = GameObject.Instantiate(cellPrefab);
			cell = gameObject.GetComponent<Cell>();
			cell.game = this;
		}

		cell.id = id;
		cell.cellType = cellType;

		cells.Add(id, cell);
	
		return cell;
	}

	public void AddCell(AddCellMessage message)
	{
		Cell cell = GetCell(message.id, message.cellType);
		cell.isMine = message.isMine;

		cell.SetSize(message.size, true);

		if (cell.isHidden)
		{
			cell.Show();
		}
		else 
		{
			cell.Init();
		}

		cell.SetName(message.name);
		cell.SetColor(message.colorR, message.colorG, message.colorB);
						
		//cell.SetPosition(message.x, message.y);
		cell.AddSnapshot(message.x, message.y, message.time);
	}

	private void HideCell(int id)
	{
		Cell cell = TryGetCell(id);
		if (cell != null)
		{
			cell.Hide();

			cells.Remove(id);
			cellCache.Enqueue(cell);

			cell.hideTime = Time.time;
		}
	}

	public void DestroyCell(DestroyCellMessage message)
	{
		HideCell(message.id);
	}

	public void HideCell(HideCellMessage message)
	{
		HideCell(message.id);
	}

	public void UpdateCell(UpdateCellMessage message)
	{
		Cell cell = TryGetCell(message.id);
		if (cell != null)
		{
			//cell.SetPosition(message.x, message.y);
			cell.AddSnapshot(message.x, message.y, message.time);
			cell.SetSize(message.size);
		}
	}

	public void UpdateCamera(UpdateCameraMessage message)
	{
		float factor = message.factor;
		float halfW = serverViewBaseX / factor;
		float halfH = serverViewBaseY / factor;

		float size = Mathf.Min(halfH, halfW / cameraScript.screenAspect);
		size = Mathf.Max(size - 275.0f, 275.0f);

		cameraScript.AddSnapshot(message.x, message.y, size, message.time);
	}

	public void RespawnPlayer(string name)
	{
		RespawnPlayerMessage message = (RespawnPlayerMessage)GetGameMessage(GameMessageTypes.RespawnPlayer);
		message.name = name;

		message.Send(client, NetDeliveryMethod.ReliableUnordered);
	}

	// Use this for initialization
	void Start()
	{
	}

	public void Connect(string name, string address, int port)
	{
		client.Start();
		
		NetOutgoingMessage approvalMessage = client.CreateMessage();
		approvalMessage.Write(name);
		client.Connect(address, port, approvalMessage);
	}

	public void Disconnect()
	{
		client.Disconnect("disconnect");
		client.Shutdown("disconnect");
	}

	private void ClearBoard()
	{
		foreach (KeyValuePair<int, Cell> pair in cells)
		{		
			pair.Value.Destroy();
		}

		cells.Clear();

		while (cellCache.Count > 0)
		{
			Cell cell = cellCache.Peek();

			cellCache.Dequeue();
			cell.Destroy();
		}

		for (int i = 0; i < maxLines; ++i)
		{
			GameObject.Destroy(lineCache[i].Key.gameObject);
		}

		lineCache.Clear();
		numLines = maxLines = 0;

		cameraScript.Reset();
	}

	private void UpdateCells()
	{
		if (!isConnected) return;

		float time = Time.time;

		while (cellCache.Count > 0)
		{
			Cell cell = cellCache.Peek();
			if (time - cell.hideTime >= 5.0f)
			{
				cellCache.Dequeue();
				cell.Destroy();
			}			
			else break;
		}
	}

	private void UpdateLines()
	{
		if (!isConnected) return;

		Vector2 position = cameraScript.GetPosition();
		Vector2 extents = cameraScript.GetExtents();

		float left = position.x - extents.x;
		float right = position.x + extents.x;

		float bottom = position.y - extents.y;
		float top = position.y + extents.y;

		const float lineOffset = 100.0f;

		float lineWidth = extents.y / 275.0f * 1.5f;
		
		int oldNumLines = numLines;
		numLines = 0;

		float time = Time.time;

		float x = (int)(left / lineOffset) * lineOffset;
		if (x + lineWidth / 2.0f < left) x += lineOffset;

		while (x - lineWidth / 2.0f <= right)
		{
			Transform line = GetLine(time);

			line.position = new Vector3(x, position.y, 0.0f);
			line.localScale = new Vector3(lineWidth, extents.y * 2.0f, 1.0f);
			
			x += lineOffset;
		}

		float y = (int)(bottom / lineOffset) * lineOffset;
		if (y + lineWidth / 2.0f < bottom) y += lineOffset;
		
		while (y - lineWidth / 2.0f <= top)
		{
			Transform line = GetLine(time);
			
			line.position = new Vector3(position.x, y, 0.0f);
			line.localScale = new Vector3(extents.x * 2.0f, lineWidth, 1.0f);
			
			y += lineOffset;
		}
		
		if (numLines < oldNumLines)	HideLines(numLines, oldNumLines);
			
		int oldMaxLines = maxLines;

		while (maxLines > 0)
		{
			if (time - lineCache[maxLines - 1].Value >= 5.0f) 
			{
				GameObject.Destroy(lineCache[--maxLines].Key.gameObject);
			}
			else break;
		}

		if (maxLines < oldMaxLines)
		{
			lineCache.RemoveRange(maxLines, oldMaxLines - maxLines);
		}
	}

	private void UpdateInput()
	{
		if (!isConnected) return;

		sendTargetTimer -= Time.deltaTime;
		if (sendTargetTimer <= 0.0f)
		{
			sendTargetTimer = 0.1f;
			SendTargetPosition();
		}

		if (Input.GetKeyDown(KeyCode.Space)) //Split cell
		{
			SplitCellMessage splitCellMessage = (SplitCellMessage)GetGameMessage(GameMessageTypes.SplitCell);
			splitCellMessage.Send(client, NetDeliveryMethod.ReliableUnordered);
		}

		if (Input.GetKeyDown(KeyCode.W)) //Eject mass
		{
			EjectMassMessage ejectMassMessage = (EjectMassMessage)GetGameMessage(GameMessageTypes.EjectMass);
			ejectMassMessage.Send(client, NetDeliveryMethod.ReliableUnordered);
		}
	}
	
	private void SendTargetPosition()
	{
		Vector3 pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

		Vector2 camPos = cameraScript.GetPosition();
		
		SetTargetMessage setTargetMessage = (SetTargetMessage)GetGameMessage(GameMessageTypes.SetTarget);
		setTargetMessage.dx = pos.x - camPos.x;
		setTargetMessage.dy = pos.y - camPos.y;
		
		setTargetMessage.Send(client, NetDeliveryMethod.ReliableSequenced);		
	}

	public Vector2 GetCameraPosition()
	{
		return cameraScript.GetPosition();
	}

	public Vector2 GetCameraExtents()
	{
		return cameraScript.GetExtents();
	}

	// Update is called once per frame
	void Update()
	{
		if (client.Connections.Count > 0)
		{
			serverTime = (float)client.Connections[0].GetRemoteTime(NetTime.Now) - 0.1f;
		}

		UpdateInput();

		NetIncomingMessage msg;
		while ((msg = client.ReadMessage()) != null)
		{
			switch (msg.MessageType)
			{
				case NetIncomingMessageType.StatusChanged:
					NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
					if (status == NetConnectionStatus.Connected)
					{
						isConnected = true;
						NetIncomingMessage message = msg.SenderConnection.RemoteHailMessage;
						borderLeft = message.ReadFloat();
						borderRight = message.ReadFloat();
						borderTop = message.ReadFloat();
						borderBottom = message.ReadFloat();
						serverViewBaseX = message.ReadInt32();
						serverViewBaseY = message.ReadInt32();

						hud.SetState(HUD.State.CONNECTED);

						Debug.Log(NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " connected!");
					}
					else if (status == NetConnectionStatus.Disconnected)
					{
						isConnected = false;
						ClearBoard();

						hud.SetState(HUD.State.UNCONNECTED);
					
						Debug.Log(NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " disconnected!");
					}
					break;
				
				case NetIncomingMessageType.Data:
					if (!isConnected) break;
				
					GameMessageTypes messageType = (GameMessageTypes)msg.ReadByte();
					
					GameMessage gameMessage = GetGameMessage(messageType);
					gameMessage.Decode(msg);

					switch (messageType)
					{
						case GameMessageTypes.AddCell:
							AddCell((AddCellMessage)gameMessage);
							break;

						case GameMessageTypes.UpdateCell:
							UpdateCell((UpdateCellMessage)gameMessage);
							break;

						case GameMessageTypes.HideCell:
							HideCell((HideCellMessage)gameMessage);
							break;

						case GameMessageTypes.DestroyCell:
							DestroyCell((DestroyCellMessage)gameMessage);
							break;

						case GameMessageTypes.UpdateCamera:
							UpdateCamera((UpdateCameraMessage)gameMessage);
							break;

						case GameMessageTypes.GameLose:
							hud.SetState(HUD.State.CONNECTED_LOSE);
							break;

						default:
							break;
					}
					break;

				default:
					break;
			}	
		}

		linearBinMinX = borderRight;
		linearBinMaxX = borderLeft;

		linearBinMinY = borderBottom;
		linearBinMaxY = borderTop;
	}

	public void UpdateLinearBinBound(float x, float y)
	{
		if (x < linearBinMinX) linearBinMinX = x;
		if (x > linearBinMaxX) linearBinMaxX = x;

		if (y < linearBinMinY) linearBinMinY = y;
		if (y > linearBinMaxY) linearBinMaxY = y;
	}

	void LateUpdate()
	{
		UpdateCells();
		UpdateLines();

		if (linearBinMinX <= linearBinMaxX && linearBinMinY <= linearBinMaxY)
		{
			linearBin.Init(linearBinMinX - 10.0f, linearBinMinY - 10.0f, linearBinMaxX + 10.0f, linearBinMaxY + 10.0f, 32.0f);
		}
	}

	void OnDestroy()
	{
		client.Disconnect("Bye");
	}
}
