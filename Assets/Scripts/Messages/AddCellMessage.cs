using Lidgren.Network;

public class AddCellMessage : GameMessage
{
    public int id;

    public float time;

    public CellType cellType;
    public bool isMine;

    public float x;
    public float y;

    public float size;

    public byte colorR;
    public byte colorG;
    public byte colorB;

    public string name;

    public AddCellMessage()
	{
		type = GameMessageTypes.AddCell;
	}

	public override void Decode(NetIncomingMessage im)
	{
        id = im.ReadInt32();
        time = im.ReadFloat();
        cellType = (CellType)im.ReadByte();
        isMine = im.ReadBoolean();
        x = im.ReadFloat();
        y = im.ReadFloat();
        size = im.ReadFloat();
        colorR = im.ReadByte();
        colorG = im.ReadByte();
        colorB = im.ReadByte();

        if (cellType == 0) //PlayerCell
        {
            name = im.ReadString();
        }
		else 
		{
			name = null;
		}
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
        om.Write(id);
        om.Write(time);
        om.Write((byte)cellType);
        om.Write(isMine);
        om.Write(x);
        om.Write(y);
        om.Write(size);
        om.Write(colorR);
        om.Write(colorG);
        om.Write(colorB);

        if (cellType == 0) //PlayerCell
        {
            om.Write(name);
        }
	}
}

