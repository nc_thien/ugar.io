using Lidgren.Network;

public class DestroyCellMessage : GameMessage
{
    public int id;

	public DestroyCellMessage()
	{
		type = GameMessageTypes.DestroyCell;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
        id = im.ReadInt32();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
        om.Write(id);
	}
}

