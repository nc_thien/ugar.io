﻿using Lidgren.Network;

public class UpdateCameraMessage : GameMessage
{
    public float time;

	public float x;
	public float y;
	
	public float factor;

	public UpdateCameraMessage()
	{
		type = GameMessageTypes.UpdateCamera;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
        time = im.ReadFloat();
		x = im.ReadFloat();
		y = im.ReadFloat();
		factor = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
        om.Write(time);
		om.Write(x);
		om.Write(y);
		om.Write(factor);
	}
}

