using Lidgren.Network;

public class HideCellMessage : GameMessage
{
    public int id;

	public HideCellMessage()
	{
		type = GameMessageTypes.HideCell;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
        id = im.ReadInt32();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
        om.Write(id);
	}
}

