using Lidgren.Network;

public class UpdateCellMessage : GameMessage
{
    public int id;

    public float time;

    public float x;
    public float y;

    public float size;

	public UpdateCellMessage()
	{
		type = GameMessageTypes.UpdateCell;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
        id = im.ReadInt32();
        time = im.ReadFloat();
        x = im.ReadFloat();
        y = im.ReadFloat();
        size = im.ReadFloat();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
        om.Write(id);
        om.Write(time);
        om.Write(x);
        om.Write(y);
        om.Write(size);
	}
}

