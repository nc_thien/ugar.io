using Lidgren.Network;

public class SetTargetMessage : GameMessage
{
    public float dx;
    public float dy;

	public SetTargetMessage()
	{
		type = GameMessageTypes.SetTarget;
	}

	public override void Decode(NetIncomingMessage im)
	{
        dx = im.ReadFloat();
        dy = im.ReadFloat();
	}

	public override void Encode(NetOutgoingMessage om)
	{
        om.Write(dx);
        om.Write(dy);
	}
}

