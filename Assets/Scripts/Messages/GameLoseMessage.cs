using Lidgren.Network;

public class GameLoseMessage : GameMessage
{
	public GameLoseMessage()
	{
		type = GameMessageTypes.GameLose;
	}

	public override void Decode(NetIncomingMessage im)
	{
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
	}
}

