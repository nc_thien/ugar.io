using Lidgren.Network;

public class RespawnPlayerMessage : GameMessage
{
	public string name;

	public RespawnPlayerMessage()
	{
		type = GameMessageTypes.RespawnPlayer;
	}
	
	public override void Decode(NetIncomingMessage im)
	{
		name = im.ReadString();
	}
	
	public override void Encode(NetOutgoingMessage om)
	{
		om.Write(name);
	}
}

