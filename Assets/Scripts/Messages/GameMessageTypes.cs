﻿public enum GameMessageTypes
{
	SetTarget = 0,
	SplitCell,
	EjectMass,
	AddCell,
	UpdateCell,
	HideCell,
	DestroyCell,
	UpdateCamera,
	GameLose,
	RespawnPlayer
}