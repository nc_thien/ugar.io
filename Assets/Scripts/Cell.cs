using UnityEngine;
using System.Collections;

public class Cell : MonoBehaviour
{
	protected Transform cachedTransform;

	protected CellRenderer cellRenderer;
	
	[HideInInspector]
	public float hideTime;

	[HideInInspector]
	public bool isHidden;

	[HideInInspector]
	public int id;

	[HideInInspector]
	public bool isMine;

	[HideInInspector]
	public float x;

	[HideInInspector]
	public float y;

	[HideInInspector]
	public float size;

	[HideInInspector]
	public Game game;

	[HideInInspector]
	public CellType cellType;

	protected float currentSize = 1.0f;
	protected float sizeVel = 0.0f;

	protected struct Snapshot
	{
		public float x;
		public float y;
		public float time;
	}

	protected const int MAX_SNAPSHOT = 5;

	protected Snapshot[] snapshots;
	protected int numSnapshots;
	protected int startSnapshotPos;

	protected float lastSnapshotTime;

	void Awake()
	{
		cachedTransform = transform;
		cellRenderer = GetComponent<CellRenderer>();

		snapshots = new Snapshot[MAX_SNAPSHOT];
		for (int i = 0; i < MAX_SNAPSHOT; ++i) snapshots[i] = new Snapshot();

		numSnapshots = 0;
		startSnapshotPos = 0;

		lastSnapshotTime = 0.0f;

		isHidden = false;
		isMine = false;

		x = y = 0.0f;
		size = 0.0f;
	}

	public virtual void Destroy()
	{
		GameObject.Destroy(gameObject);
		cellRenderer.Hide();
	}

	public virtual void Hide()
	{
		if (!isHidden)
		{
			isHidden = true;

			gameObject.SetActive(false);
			cellRenderer.Hide();
		}
	}

	public void Show()
	{
		if (isHidden)
		{
			isHidden = false;
			gameObject.SetActive(true);

			numSnapshots = 0;
			startSnapshotPos = 0;

			lastSnapshotTime = 0.0f;

			cellRenderer.Show(cellType != CellType.FOOD);
		}
	}

	public void Init()
	{
		cellRenderer.Init(game, this, cellType != CellType.FOOD); 
	}

	public void AddSnapshot(float x, float y, float time)
	{
		if (time <= lastSnapshotTime) return;
		lastSnapshotTime = time;

		if (numSnapshots == MAX_SNAPSHOT)
		{
			snapshots[startSnapshotPos].x = x;
			snapshots[startSnapshotPos].y = y;
			snapshots[startSnapshotPos].time = time;

			startSnapshotPos = (startSnapshotPos + 1) % MAX_SNAPSHOT;
		}
		else 
		{
			int pos = (startSnapshotPos + numSnapshots) % MAX_SNAPSHOT;

			snapshots[pos].x = x;
			snapshots[pos].y = y;
			snapshots[pos].time = time;
			
			numSnapshots++;
		}
	}

	public void UpdateState(float time)
	{
		if (numSnapshots == 1 || time <= snapshots[startSnapshotPos].time)
		{
			SetPosition(snapshots[startSnapshotPos].x, snapshots[startSnapshotPos].y);
		}
		else 
		{
			int pos = startSnapshotPos;

			for (int i = 1; i < numSnapshots; ++i)
			{
				pos = (pos + 1) % MAX_SNAPSHOT;

				if (time >= snapshots[pos].time) 
				{
					startSnapshotPos = pos;

					i--;
					numSnapshots--;
				}
				else break;
			}

			if (numSnapshots == 1)
			{
				SetPosition(snapshots[startSnapshotPos].x, snapshots[startSnapshotPos].y);
			}
			else
			{
				float ratio = (time - snapshots[startSnapshotPos].time) / (snapshots[pos].time - snapshots[startSnapshotPos].time);
				SetPosition(Mathf.Lerp(snapshots[startSnapshotPos].x, snapshots[pos].x, ratio), Mathf.Lerp(snapshots[startSnapshotPos].y, snapshots[pos].y, ratio));
			}
		}
	}

	public void SetName(string name)
	{
		Game.SkinInfo skinInfo = game.GetSkinInfo(name);
		if (skinInfo == null)
		{
			cellRenderer.ShowName(name);
			cellRenderer.SetSkin(null);

			return;
		}

		if (skinInfo.hasName) cellRenderer.ShowName(name);
		cellRenderer.SetSkin(skinInfo.texture, skinInfo.isTransparent);
	}

	public void SetPosition(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	public void SetSize(float size, bool force = false)
	{
		this.size = size;
		if (force)
		{
			currentSize = size;
			sizeVel = 0.0f;
		}
	}

	public float GetCurrentSize()
	{
		return currentSize;
	}

	public virtual void SetColor(byte r, byte g, byte b)
	{
		cellRenderer.SetColor(new Color(r / 255.0f, g / 255.0f, b / 255.0f));
	}

	// Use this for initialization
	void Start()
	{
	}
	
	// Update is called once per frame
	public virtual void Update ()
	{
		UpdateState(game.serverTime);
		
		cachedTransform.position = new Vector3(x, y, -size);
		
		currentSize = Mathf.SmoothDamp(currentSize, size, ref sizeVel, 0.2f);
		
		cellRenderer.UpdateMesh(game.linearBin);
	}

	public void LateUpdate()	
	{
		cellRenderer.UpdatePointBin(game.linearBin);
	}
}

