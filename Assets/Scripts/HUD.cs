using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour
{
	public enum State
	{
		UNCONNECTED,
		CONNECTING,
		CONNECTED,
		CONNECTED_LOSE,
	}

	private State state;

	[HideInInspector]
	public Game game;

	public RectTransform panel;

	public UnityEngine.UI.InputField nameInput;
	public UnityEngine.UI.InputField serverAddressInput;
	public UnityEngine.UI.InputField serverPortInput;

	public UnityEngine.UI.Button startButton;
	public UnityEngine.UI.Button disconnectButton;

	private string currentAddress = string.Empty;
	private int currentPort = 0;

	// Use this for initialization
	void Start ()
	{
		serverAddressInput.text = "127.0.0.1";
		serverPortInput.text = "14242";

		state = State.UNCONNECTED;
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void SetState(State state)
	{
		if (this.state != state)
		{
			this.state = state;

			switch (state)
			{
			case State.UNCONNECTED:
				panel.gameObject.SetActive(true);
				disconnectButton.gameObject.SetActive(false);
				startButton.enabled = true;
				break;

			case State.CONNECTING:
				panel.gameObject.SetActive(true);
				disconnectButton.gameObject.SetActive(false);
				startButton.enabled = false;
				break;

			case State.CONNECTED:
				panel.gameObject.SetActive(false);
				disconnectButton.gameObject.SetActive(true);
				break;

			case State.CONNECTED_LOSE:
				panel.gameObject.SetActive(true);
				disconnectButton.gameObject.SetActive(false);
				startButton.enabled = true;
				break;

			default:
				break;
			}
		}
	}

	public void StartGame()
	{
		int port = 0;

		try 
		{
			port = int.Parse(serverPortInput.text);
		} catch (System.Exception) 
		{
			port = 0;
		}

		string address = serverAddressInput.text;

		if (port > 0 && !string.IsNullOrEmpty(address))
		{
			string name = nameInput.text;
			if (name.Length > 15) name = name.Substring(15);

			if (state == State.CONNECTED_LOSE)
			{
				if (port == currentPort && string.Equals(address, currentAddress))
				{
					game.RespawnPlayer(name);
					SetState(State.CONNECTED);
				}
				else state = State.UNCONNECTED;
			}

			if (state == State.UNCONNECTED)
			{
				currentAddress = address;
				currentPort = port;

				game.Connect(name, address, port);
				SetState(State.CONNECTING);
			}
		}
	}

	public void DisconnectGame()
	{
		game.Disconnect();
	}
}

