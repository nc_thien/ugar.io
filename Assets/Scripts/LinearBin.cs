using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LinearBin<T>
{
	private int capacity;

	private int w;
	private int h;
	private int count;

	private float x0, y0;

	private float cellSize;

	private List<LinkedList<T>> bins;

	public LinearBin()
	{
		capacity = 0;
		bins = new List<LinkedList<T>>();
	}

	public void Init(float x0, float y0, float x1, float y1, float cellSize)
	{
		this.cellSize = cellSize;

		this.x0 = x0;
		this.y0 = y0;

		w = Mathf.FloorToInt((x1 - x0) / cellSize) + 1;
		h = Mathf.FloorToInt((y1 - y0) / cellSize) + 1;

		count = w * h;

		if (count > capacity)
		{
			for (int i = capacity; i < count; ++i)
			{
				bins.Add(new LinkedList<T>());
			}

			capacity = count;
		}
	}

	public LinkedList<T> GetBin(float x, float y)
	{
		int ix = Mathf.Clamp(Mathf.FloorToInt((x - x0) / cellSize) + 1, 0, w - 1);
		int iy = Mathf.Clamp(Mathf.FloorToInt((y - y0) / cellSize) + 1, 0, h - 1);

		return bins[ix + iy * w];
	}

	public void GetBinIndex(float x, float y, out int ix, out int iy)
	{
		ix = Mathf.Clamp(Mathf.FloorToInt((x - x0) / cellSize) + 1, 0, w - 1);
		iy = Mathf.Clamp(Mathf.FloorToInt((y - y0) / cellSize) + 1, 0, h - 1);
	}

	public LinkedList<T> GetBin(int ix, int iy)
	{
		return bins[ix + iy * w];
	}
}

