﻿public enum CellType
{
	PLAYER_CELL = 0,
	FOOD = 1,
	VIRUS = 2,
	EJECTED_MASS = 3,
	INVALID = -1
}
