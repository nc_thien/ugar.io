using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class CellRenderer : MonoBehaviour
{
	public class PointInfo
	{
		public int cellId;

		public float accum;
		public float h;

		public float x, y;

		public LinkedList<PointInfo> bin = null;
		public LinkedListNode<PointInfo> binNode = null;

		public void UpdateBin(LinearBin<PointInfo> linearBin)
		{
			LinkedList<PointInfo> newBin = linearBin.GetBin(x, y);
			if (newBin != bin)
			{
				if (bin != null) bin.Remove(binNode);

				bin = newBin;
				binNode = bin.AddLast(this);
			}
		}

		public void Hide()
		{
			if (bin != null)
			{
				bin.Remove(binNode);
			
				bin = null;
				binNode = null;
			}
		}
	}

	private static Queue<PointInfo> pointInfoPool = new Queue<PointInfo>();

	private static PointInfo GetPointInfo()
	{
		if (pointInfoPool.Count > 0)
		{
			return pointInfoPool.Dequeue();
		}
		else 
		{
			return new PointInfo();
		}
	}

	private static void RecyclePointInfo(PointInfo pointInfo)
	{
		pointInfo.bin.Remove(pointInfo.binNode);

		pointInfo.bin = null;
		pointInfo.binNode = null;
	
		pointInfoPool.Enqueue(pointInfo);
	}

	private Vector3[] vertices;
	private Vector2[] uvs;
	private int[] triangles;
	private int[] borderTriangles;

	private Cell cell;
	private Game game;

	private int numPoints;

	private Mesh mesh;

	private List<PointInfo> pointInfos;

	private MeshFilter meshFilter;
	private MeshRenderer meshRenderer;

	public GameObject nameDisplayPrefab;
	private CellNameDisplay nameDisplay;

	private bool hasBorder;
	private bool hasName;

	public Material[] materials;
	
	void Awake()
	{
		pointInfos = new List<PointInfo>();

		meshFilter = GetComponent<MeshFilter>();
		meshRenderer = GetComponent<MeshRenderer>();

		mesh = new Mesh();
		meshFilter.mesh = mesh;

		hasBorder = false;
		hasName = false;

		nameDisplay = null;
 	}

	public void Init(Game game, Cell cell, bool hasBorder)
	{
		this.game = game;
		this.cell = cell;

		this.hasBorder = hasBorder;

		numPoints = GetNumPoints();
		
		if (hasBorder)
		{
			vertices = new Vector3[2 * numPoints + 1];
			uvs = new Vector2[2 * numPoints + 1];
		}
		else 
		{
			vertices = new Vector3[numPoints + 1];
			uvs = new Vector2[numPoints + 1];
		}
		
		triangles = new int[numPoints * 3];
		
		if (hasBorder)
		{
			borderTriangles = new int[numPoints * 3 * 2];
		}
		else 
		{
			borderTriangles = null;
		}
		
		vertices[0] = Vector3.zero;
		uvs[0] = Vector2.one * 0.5f;
		
		for (int i = 0; i < numPoints; ++i) AddStartPointInfo();
	}

	public void Show(bool hasBorder)
	{
		ShowBorder(hasBorder);

		for (int i = 0; i < numPoints; ++i)
		{
			PointInfo pointInfo = pointInfos[i];
			
			pointInfo.cellId = cell.id;
			pointInfo.h = cell.GetCurrentSize();
			pointInfo.accum = UnityEngine.Random.Range(0.0f, 1.0f) - 0.5f;
		}
	}
	
	public void Hide()
	{
		mesh.Clear();
		
		for (int i = 0; i < numPoints; ++i)
		{
			pointInfos[i].Hide();
		}
	}

	public void SetColor(Color color)
	{
		meshRenderer.materials[0].color = color;

		if (hasBorder)
		{
			meshRenderer.materials[1].color = Color.Lerp(color, Color.black, 0.1f);
		}
	}

	public void ShowBorder(bool value)
	{
		if (hasBorder != value)
		{
			hasBorder = value;

			if (hasBorder)
			{
				Array.Resize(ref vertices, 2 * numPoints + 1);
				Array.Resize(ref uvs, 2 * numPoints + 1);

				borderTriangles = new int[numPoints * 3 * 2];
			}
			else 
			{
				Array.Resize(ref vertices, numPoints + 1);
				Array.Resize(ref uvs, numPoints + 1);

				borderTriangles = null;
			}
		}
	}

	public void ShowName(string name)
	{
		bool value = !(string.IsNullOrEmpty(name));

		if (hasName != value)
		{
			hasName = value;

			if (hasName)
			{
				GameObject obj = GameObject.Instantiate(nameDisplayPrefab) as GameObject;
				obj.transform.SetParent(transform);
				obj.transform.localPosition = new Vector3(0.0f, 0.0f, -0.1f);
				obj.transform.localRotation = Quaternion.identity;
				obj.transform.localScale = Vector3.one;

				nameDisplay = obj.GetComponent<CellNameDisplay>();
				nameDisplay.cell = cell;
			}
			else 
			{
				GameObject.Destroy(nameDisplay.gameObject);
				nameDisplay = null;
			}
		}

		if (value)
		{
			nameDisplay.SetName(name);
		}
	}

	public void SetSkin(Texture texture, bool isTransparent = false)
	{
		if (texture == null)
		{
			meshRenderer.materials = new Material[]{materials[0], materials[0]}; //solid
		}
		else 
		{
			if (!isTransparent) meshRenderer.materials = new Material[]{materials[1], materials[0]}; //skin
			else meshRenderer.materials = new Material[]{materials[2], materials[0]}; //skin color

			meshRenderer.materials[0].mainTexture = texture;
		}
	}

	// Use this for initialization
	void Start()
	{
	
	}

	private void AddStartPointInfo()
	{
		PointInfo pointInfo = GetPointInfo();
		pointInfo.cellId = cell.id;
		pointInfo.h = cell.GetCurrentSize();
		pointInfo.accum = UnityEngine.Random.Range(0.0f, 1.0f) - 0.5f;

		pointInfos.Add(pointInfo);
	}

	private int GetNumPoints()
	{
		int minNumPoints = 10;

		float size = cell.GetCurrentSize();

		if (size < 20.0f) minNumPoints = 5;
		if (cell.cellType == CellType.VIRUS) minNumPoints = 30;

		float val = (size / game.GetCameraExtents().y * 275.0f) * 0.5f;
		return Mathf.Max(Mathf.FloorToInt(val), minNumPoints);
	}

	private void SetNumPoints(int newNumPoints)
	{
		if (newNumPoints != numPoints)
		{
			if (newNumPoints < numPoints)
			{
				for (int i = newNumPoints; i < numPoints; ++i) RecyclePointInfo(pointInfos[i]);
				pointInfos.RemoveRange(newNumPoints, numPoints - newNumPoints);
			}
			else 
			{
				for (int i = numPoints; i < newNumPoints; ++i) AddStartPointInfo();
			}

			numPoints = newNumPoints;

			if (hasBorder)
			{
				Array.Resize(ref vertices, 2 * numPoints + 1);
				Array.Resize(ref uvs, 2 * numPoints + 1);

				Array.Resize(ref borderTriangles, numPoints * 3 * 2);
			}
			else 
			{
				Array.Resize(ref vertices, numPoints + 1);
				Array.Resize(ref uvs, numPoints + 1);
			}

			Array.Resize(ref triangles, numPoints * 3);
		}	
	}

	public void UpdatePointBin(LinearBin<PointInfo> linearBin)
	{
		for (int i = 0; i < numPoints; ++i)
		{
			pointInfos[i].UpdateBin(linearBin);
		}
	}
	
	public void UpdateMesh(LinearBin<PointInfo> linearBin)
	{
		float size = cell.GetCurrentSize();

		int newNumPoints = GetNumPoints();
		SetNumPoints(newNumPoints);

		float lineWidth = game.GetCameraExtents().y / 275.0f * 4.0f;

		float start = pointInfos[0].accum;
		float prev = pointInfos[numPoints - 1].accum;
		float next;

		float current;

		for (int i = 0; i < numPoints; ++i)
		{
			float old = pointInfos[i].accum;
			next = (i < numPoints - 1 ? pointInfos[i + 1].accum : start);

			current = Mathf.Clamp((old + UnityEngine.Random.Range(0.0f, 1.0f) - 0.5f) * 0.7f, -10.0f, 10.0f);

			pointInfos[i].accum = (prev + next + current * 8.0f) / 10.0f;

			prev = old;
		}

		start = pointInfos[0].h;
		prev = pointInfos[numPoints - 1].h;

		float dx, dy;

		float x0 = cell.x;
		float y0 = cell.y;

		int id = cell.id;

		float phase = (id / 1000.0f + Time.time / 10.0f) % (Mathf.PI * 2.0f);
		
		for (int i = 0; i < numPoints; ++i)
		{
			PointInfo pointInfo = pointInfos[i];

			float old = pointInfo.h;
			next = (i < numPoints - 1 ? pointInfos[i + 1].h : start);

			float x = pointInfo.x;
			float y = pointInfo.y;

			if (size > 15.0f)
			{
				bool hasNearPoint = false;

				if (cell.game.IsInBound(x, y))
				{
					int ix0, iy0;
					int ix1, iy1;

					linearBin.GetBinIndex(x - 5.0f, y - 5.0f, out ix0, out iy0);
					linearBin.GetBinIndex(x + 5.0f, y + 5.0f, out ix1, out iy1);

					for (int ix = ix0; ix <= ix1 && !hasNearPoint; ++ix)
					{
						for (int iy = iy0; iy <= iy1; ++iy)
						{
							LinkedList<PointInfo> bin = linearBin.GetBin(ix, iy);
							for (LinkedListNode<PointInfo> it = bin.First; it != null; it = it.Next)
							{
								PointInfo other = it.Value;
								if (other.cellId != id)
								{
									dx = other.x - x;
									dy = other.y - y;

									if (dx * dx + dy * dy < 25.0f)
									{
										hasNearPoint = true;
										break;
									}
								}
							}
						}
					}
				}
				else 
				{
					hasNearPoint = true;
				}

				if (hasNearPoint) 
				{
					pointInfos[i].accum = Mathf.Min(pointInfos[i].accum, 0.0f) - 1.0f;
				}
			}

			current = Mathf.Max(old + pointInfos[i].accum, 0.0f);
			current = (current * 12.0f + size) / 13.0f;
			
			current = (prev + next + current * 8.0f) / 10.0f;
			
			float angle = Mathf.PI * 2.0f / numPoints;

			pointInfo.h = current;

			float sina = Mathf.Sin(i * angle + phase);
			float cosa = Mathf.Cos(i * angle + phase);

			if (cell.cellType == CellType.VIRUS)
			{
				if (i % 2 == 0) current += 5.0f;
			}

			x = cosa * current;
			y = sina * current;

			cell.game.UpdateLinearBinBound(x + x0, y + y0);

			pointInfo.x = x + x0;
			pointInfo.y = y + y0;

			dx = cosa * lineWidth / 2.0f;
			dy = sina * lineWidth / 2.0f;

			Vector2 uv = new Vector2(((x - dx) / size + 1.0f) * 0.5f, ((y - dy) / size + 1.0f) * 0.5f);

			if (hasBorder)
			{
				vertices[i * 2 + 1] = new Vector3(x - dx, y - dy, 0.0f);
				vertices[i * 2 + 2] = new Vector3(x + dx, y + dy, 0.0f);
	
				uvs[i * 2 + 1] = uv;
				uvs[i * 2 + 2] = uv;
			}
			else 
			{
				vertices[i + 1] = new Vector3(x + dx, y + dy, 0.0f);
				uvs[i + 1] = uv;
			}

			prev = old;
		}

		for (int i = 0; i < numPoints; ++i)
		{
			int nextId = (i + 1) % numPoints;

			if (hasBorder)
			{
				triangles[3 * i] = 0;
				triangles[3 * i + 1] = nextId * 2 + 1;
				triangles[3 * i + 2] = i * 2 + 1;
	
				borderTriangles[6 * i] = nextId * 2 + 1;
				borderTriangles[6 * i + 1] = nextId * 2 + 2;
				borderTriangles[6 * i + 2] = i * 2 + 2;
				borderTriangles[6 * i + 3] = nextId * 2 + 1;
				borderTriangles[6 * i + 4] = i * 2 + 2;
				borderTriangles[6 * i + 5] = i * 2 + 1;
			}
			else 
			{
				triangles[3 * i] = 0;
				triangles[3 * i + 1] = nextId + 1;
				triangles[3 * i + 2] = i + 1;
			}
		}

		mesh.Clear();

		mesh.subMeshCount = (hasBorder ? 2 : 1);

		mesh.vertices = vertices;
		mesh.uv = uvs;
		mesh.SetTriangles(triangles, 0);

		if (hasBorder)
		{
			mesh.SetTriangles(borderTriangles, 1);
		}
	}
}

