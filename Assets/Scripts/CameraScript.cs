using UnityEngine;
using System.Collections;

public class CameraScript : MonoBehaviour
{
	[HideInInspector]
	public Game game;

	private Transform cachedTransform;
	private Camera cachedCamera;

	private float currentSize = 275.0f;
	private float sizeVel = 0.0f;

	[HideInInspector]
	public float screenAspect;

	private Vector2 position;
	private Vector2 extents;

	protected struct Snapshot
	{
		public float x;
		public float y;
		public float size;
		public float time;
	}

	protected const int MAX_SNAPSHOT = 5;
	
	protected Snapshot[] snapshots;
	protected int numSnapshots;
	protected int startSnapshotPos;
	
	protected float lastSnapshotTime;
		
	void Awake()
	{
		cachedTransform = transform;
		cachedCamera = GetComponent<Camera>();

		snapshots = new Snapshot[MAX_SNAPSHOT];
		for (int i = 0; i < MAX_SNAPSHOT; ++i) snapshots[i] = new Snapshot();
		
		numSnapshots = 0;
		startSnapshotPos = 0;
		
		lastSnapshotTime = 0.0f;
	}

	// Use this for initialization
	void Start()
	{
		Vector3 pos = cachedTransform.position;

		currentSize = cachedCamera.orthographicSize;

		position = new Vector2(pos.x, pos.y);

		screenAspect = (float)Screen.width / Screen.height;
		extents = new Vector2(currentSize * screenAspect, currentSize);
	}
	
	public void Reset()
	{
		numSnapshots = 0;
		startSnapshotPos = 0;
		
		lastSnapshotTime = 0.0f;

		SetPosition(0.0f, 0.0f);
		SetSize(275.0f, true);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (numSnapshots > 0)
		{
			UpdateState(game.serverTime);
		}
	}

	public void AddSnapshot(float x, float y, float size, float time)
	{
		if (time <= lastSnapshotTime) return;
		lastSnapshotTime = time;

		if (numSnapshots == MAX_SNAPSHOT)
		{
			snapshots[startSnapshotPos].x = x;
			snapshots[startSnapshotPos].y = y;
			snapshots[startSnapshotPos].size = size;
			snapshots[startSnapshotPos].time = time;
			
			startSnapshotPos = (startSnapshotPos + 1) % MAX_SNAPSHOT;
		}
		else 
		{
			int pos = (startSnapshotPos + numSnapshots) % MAX_SNAPSHOT;
			
			snapshots[pos].x = x;
			snapshots[pos].y = y;
			snapshots[pos].size = size;
			snapshots[pos].time = time;
			
			numSnapshots++;
		}
	}
	
	public void UpdateState(float time)
	{
		if (numSnapshots == 1 || time <= snapshots[startSnapshotPos].time)
		{
			SetPosition(snapshots[startSnapshotPos].x, snapshots[startSnapshotPos].y);
			SetSize(snapshots[startSnapshotPos].size);
		}
		else 
		{
			int pos = startSnapshotPos;
			
			for (int i = 1; i < numSnapshots; ++i)
			{
				pos = (pos + 1) % MAX_SNAPSHOT;
				
				if (time >= snapshots[pos].time) 
				{
					startSnapshotPos = pos;
					
					i--;
					numSnapshots--;
				}
				else break;
			}
			
			if (numSnapshots == 1)
			{
				SetPosition(snapshots[startSnapshotPos].x, snapshots[startSnapshotPos].y);
				SetSize(snapshots[startSnapshotPos].size);
			}
			else
			{
				float ratio = (time - snapshots[startSnapshotPos].time) / (snapshots[pos].time - snapshots[startSnapshotPos].time);
				SetPosition(Mathf.Lerp(snapshots[startSnapshotPos].x, snapshots[pos].x, ratio), Mathf.Lerp(snapshots[startSnapshotPos].y, snapshots[pos].y, ratio));
				SetSize(Mathf.Lerp(snapshots[startSnapshotPos].size, snapshots[pos].size, ratio));
			}
		}
	}

	public void SetPosition(float x, float y)
	{
		cachedTransform.position = new Vector3(x, y, 10.0f);

		position.x = x;
		position.y = y;
	}

	public void SetSize(float size, bool force = false)
	{
		if (force) 
		{
			currentSize = size;
			sizeVel = 0.0f;
		}
		else currentSize = Mathf.SmoothDamp(currentSize, size, ref sizeVel, 0.5f);
		cachedCamera.orthographicSize = currentSize;

		extents.x = currentSize * screenAspect;
		extents.y = currentSize;
	}

	public Vector2 GetPosition()
	{
		return position;
	}

	public Vector2 GetExtents()
	{
		return extents;
	}
}
