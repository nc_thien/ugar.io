﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class EjectedMass : Cell
    {
        private float size;
        private float squareSize;

        public LinkedListNode<EjectedMass> staticEjectedMassNode = null;
        
        public EjectedMass(int id, Position position, float mass, GameServer gameServer)
            : base(id, position, mass, gameServer)
        {
            cellType = CellType.EJECTED_MASS;

            size = base.GetSize();
            squareSize = base.GetSquareSize();
        }

        public override float GetSize()
        {
            return size;
        }

        public override float GetSquareSize()
        {
            return squareSize;
        }

        public override bool ShouldSendUpdate()
        {
            return moveEngineTicks > 0 || forceSendUpdate;
        }

        public override void OnRemove()
        {
            base.OnRemove();

            if (staticEjectedMassNode != null)
            {
                gameServer.staticEjectedMasses.Remove(staticEjectedMassNode);
                staticEjectedMassNode = null;
            }
        }

        public override void StartAutoMove()
        {
            base.StartAutoMove();

            if (staticEjectedMassNode != null)
            {
                gameServer.staticEjectedMasses.Remove(staticEjectedMassNode);
                staticEjectedMassNode = null;
            }
        }

        public override void StopAutoMove()
        {
            base.StopAutoMove();
            forceSendUpdate = true;

            staticEjectedMassNode = gameServer.staticEjectedMasses.AddLast(this);
        }

        public override void AutoMove()
        {
            base.AutoMove();

            if (gameServer.virusCells.Count < Config.VirusMaxAmount)
            {
                Virus consumer = gameServer.GetVirusConsumer(this);
                if (consumer != null)
                {
                    consumer.Feed(this);
                    gameServer.RemoveCell(this);
                }
            }
        }
    }
}
