﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Lidgren.Network;

namespace Server
{
    public class Player
    {
        public NetConnection connection;
        public GameServer gameServer;

        public string name;
        public Color color;

        public LinkedList<PlayerCell> cells;

        public List<Cell> visibleCells;

        public List<Cell> cellAddList;
        public List<Cell> cellRemoveList;
        
        public List<Cell> newVisibleCells;

        public List<Cell> addList;
        public List<Cell> updateList;
        public List<Cell> hideList;
        public List<Cell> destroyList;
        
        public Box viewBox;

        public float targetX;
        public float targetY;

        public float targetDx;
        public float targetDy;

        public int tickViewBox = 0;

        private Position center;
        private Size sightRange;
        private float factor;

        public bool splitCellFlag = false;
        public bool ejectMassFlag = false;

        public int disconnectCountdown = 0;

        public bool isActive;

        public Player(NetConnection connection, GameServer gameServer)
        {
            this.connection = connection;
            this.gameServer = gameServer;

            cells = new LinkedList<PlayerCell>();
            visibleCells = new List<Cell>();

            cellAddList = new List<Cell>();
            cellRemoveList = new List<Cell>();

            newVisibleCells = new List<Cell>();

            addList = new List<Cell>();
            updateList = new List<Cell>();
            hideList = new List<Cell>();
            destroyList = new List<Cell>();
            
            viewBox = new Box();

            center = new Position(3000.0f, 3000.0f);
            sightRange = new Size(0.0f, 0.0f);

            factor = 1.0f;

            targetDx = targetDy = 0.0f;

            viewBox.Set(center, sightRange);

            isActive = false;
        }

        public void SetTarget(float dx, float dy)
        {
            targetDx = dx;
            targetDy = dy;
        }
            
        private void UpdateViewBox()
        {
            float totalSize = 0.0f;
            float totalMass = 0.0f;

            float x = 0.0f;
            float y = 0.0f;

            for (LinkedListNode<PlayerCell> it = cells.First; it != null; it = it.Next)
            {
                PlayerCell cell = it.Value;
                totalSize += cell.GetSize();
                totalMass += cell.mass;

                x += cell.position.x * cell.mass;
                y += cell.position.y * cell.mass;
            }

            int numCells = cells.Count;
            if (numCells > 0)
            {
                factor = (float)Math.Pow(Math.Min(64.0f / totalSize, 1.0f), 0.4f);
                sightRange.w = Config.ServerViewBaseX / factor;
                sightRange.h = Config.ServerViewBaseY / factor;

                x /= totalMass;
                y /= totalMass;

                center.x = x;
                center.y = y;

                targetX = x + targetDx;
                targetY = y + targetDy;
            }

            viewBox.Set(center, sightRange);
        }

        private void SendGameLose()
        {
            GameLoseMessage gameLoseMessage = (GameLoseMessage)gameServer.GetGameMessage(GameMessageTypes.GameLose);
            gameLoseMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.ReliableUnordered);
        }

        private void SendCameraState()
        {
            float time = (float)gameServer.time;

            UpdateCameraMessage updateMessage = (UpdateCameraMessage)gameServer.GetGameMessage(GameMessageTypes.UpdateCamera);
            updateMessage.time = time;
            updateMessage.x = center.x;
            updateMessage.y = center.y;
            updateMessage.factor = factor;

            updateMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.Unreliable);
        }

        private void SendCellStates()
        {
            float time = (float)gameServer.time;

            AddCellMessage addMessage = (AddCellMessage)gameServer.GetGameMessage(GameMessageTypes.AddCell);
            addMessage.time = time;

            int numCells = addList.Count;
            for (int i = 0; i < numCells; ++i)
            {
                Cell cell = addList[i];
                addMessage.id = cell.id;
                addMessage.isMine = (cell.owner == this);
                addMessage.cellType = cell.cellType;
                addMessage.name = cell.GetName();
                addMessage.x = cell.position.x;
                addMessage.y = cell.position.y;
                addMessage.size = cell.GetSize();
                addMessage.colorR = cell.color.r;
                addMessage.colorG = cell.color.g;
                addMessage.colorB = cell.color.b;

                addMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.ReliableOrdered);
            }

            UpdateCellMessage updateMessage = (UpdateCellMessage)gameServer.GetGameMessage(GameMessageTypes.UpdateCell);
            updateMessage.time = time;

            numCells = updateList.Count;
            for (int i = 0; i < numCells; ++i)
            {
                Cell cell = updateList[i];
                updateMessage.id = cell.id;
                updateMessage.x = cell.position.x;
                updateMessage.y = cell.position.y;
                updateMessage.size = cell.GetSize();

                if (cell.forceSendUpdate)
                {
                    cell.forceSendUpdate = false;
                    updateMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.ReliableUnordered);
                }
                else
                {
                    updateMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.Unreliable);
                }
            }

            DestroyCellMessage destroyMessage = (DestroyCellMessage)gameServer.GetGameMessage(GameMessageTypes.DestroyCell);

            numCells = destroyList.Count;
            for (int i = 0; i < numCells; ++i)
            {
                Cell cell = destroyList[i];
                destroyMessage.id = cell.id;

                destroyMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.ReliableOrdered);
            }

            HideCellMessage hideMessage = (HideCellMessage)gameServer.GetGameMessage(GameMessageTypes.HideCell);

            numCells = hideList.Count;
            for (int i = 0; i < numCells; ++i)
            {
                Cell cell = hideList[i];
                hideMessage.id = cell.id;

                hideMessage.Send(gameServer.server, connection, Lidgren.Network.NetDeliveryMethod.ReliableOrdered);
            }
        }

        public void Update()
        {
            if (isActive)
            {
                if (cells.Count == 0)
                {
                    isActive = false;
                    SendGameLose();
                }
            }

            if (splitCellFlag)
            {
                if (isActive) gameServer.SplitCell(this);
                splitCellFlag = false;
            }

            if (ejectMassFlag)
            {
                if (isActive) gameServer.EjectMass(this);
                ejectMassFlag = false;
            }

            if (isActive) UpdateViewBox();
            
            if (tickViewBox <= 0)  // Update visible cells every 400 ms
            {
                for (LinkedListNode<Cell> it = gameServer.cells.First; it != null; it = it.Next)
                {
                    Cell cell = it.Value;
                    if (cell.CheckVisible(viewBox)) newVisibleCells.Add(cell);
                }

                int removeIndex = 0;
                int removeCount = cellRemoveList.Count;

                int newVisibleIndex = 0;
                int newVisibleCount = newVisibleCells.Count;

                int numVisibleCells = visibleCells.Count;
                for (int i = 0; i < numVisibleCells; ++i)
                {
                    Cell cell = visibleCells[i];
                    int id = cell.id;

                    bool valid = false;

                    while (newVisibleIndex < newVisibleCount)
                    {
                         Cell otherCell = newVisibleCells[newVisibleIndex];
                         if (otherCell.id < id)
                         {
                             addList.Add(otherCell);
                         }
                         else if (otherCell.id == id)
                         {
                             valid = true;
                             if (otherCell.ShouldSendUpdate()) updateList.Add(otherCell);

                             newVisibleIndex++;
                             break;
                         }
                         else break;

                         newVisibleIndex++;
                    }

                    if (!valid)
                    {
                        valid = true;

                        while (removeIndex < removeCount)
                        {
                            Cell otherCell = cellRemoveList[removeIndex];
                            if (otherCell.id == id)
                            {
                                valid = false;

                                removeIndex++;
                                break;
                            }
                            else if (otherCell.id > id) break;

                            removeIndex++;
                        }

                        if (valid) hideList.Add(cell);
                        else destroyList.Add(cell);
                    }
                }

                while (newVisibleIndex < newVisibleCount)
                {
                    Cell otherCell = newVisibleCells[newVisibleIndex];
                    addList.Add(otherCell);

                    newVisibleIndex++;
                }

                tickViewBox = 8;
            }
            else
            {
                tickViewBox--;

                int removeIndex = 0;
                int removeCount = cellRemoveList.Count;

                int numVisibleCells = visibleCells.Count;
                for (int i = 0; i < numVisibleCells; ++i)
                {
                    Cell cell = visibleCells[i];
                    int id = cell.id;

                    bool valid = true;

                    while (removeIndex < removeCount)
                    {
                        Cell otherCell = cellRemoveList[removeIndex];
                        if (otherCell.id == id)
                        {
                            valid = false;

                            removeIndex++;
                            break;
                        }
                        else if (otherCell.id > id) break;

                        removeIndex++;
                    }

                    if (!valid)
                    {
                        destroyList.Add(cell);
                        continue;
                    }

                    newVisibleCells.Add(cell);
                    if (cell.ShouldSendUpdate()) updateList.Add(cell);
                }

                int addCount = cellAddList.Count;
                for (int i = 0; i < addCount; ++i)
                {
                    Cell cell = cellAddList[i];
                    int id = cell.id;

                    bool valid = true;

                    while (removeIndex < removeCount)
                    {
                        Cell otherCell = cellRemoveList[removeIndex];
                        if (otherCell.id == id)
                        {
                            valid = false;

                            removeIndex++;
                            break;
                        }
                        else if (otherCell.id > id) break;

                        removeIndex++;
                    }

                    if (!valid) continue;

                    newVisibleCells.Add(cell);
                    addList.Add(cell);
                }
            }

            cellAddList.Clear();
            cellRemoveList.Clear();

            List<Cell> tmp = visibleCells;
            visibleCells = newVisibleCells;
            newVisibleCells = tmp;

            newVisibleCells.Clear();

            if (isActive) SendCameraState();
            SendCellStates();

            addList.Clear();
            updateList.Clear();
            hideList.Clear();
            destroyList.Clear();
        }
    }
}
