﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using Lidgren.Network;

using Microsoft.Test.CommandLineParsing;

namespace Server
{
    public struct Position
    {
        public Position(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public float x;
        public float y;
    }

    public struct Size
    {
        public Size(float w, float h)
        {
            this.w = w;
            this.h = h;
        }

        public float w;
        public float h;
    }

    public class Box
    {
        public Position center;
        public Size extents;

        public float top;
        public float bottom;

        public float left;
        public float right;

        public void Set(Position center, Size extents)
        {
            this.center = center;
            this.extents = extents;

            left = center.x - extents.w;
            right = center.x + extents.w;

            top = center.y - extents.h;
            bottom = center.y + extents.h;
        }
    }

    public struct Color
    {
        public Color(byte r, byte g, byte b)
        {
            this.r = r;
            this.g = g;
            this.b = b;
        }

        public byte r;
        public byte g;
        public byte b;
    }
    
    public class GameServer
    {
        private const int NUM_CONTACT_RESOLVE_ITERATIONS = 3;

        private List<Color> colors;

        public List<Player> players;
        public List<Player> disconnectPlayers;

        public LinkedList<Cell> cells;

        public LinkedList<Cell> autoMoveCells;

        public LinkedList<PlayerCell> playerCells;
        public LinkedList<Food> foodCells;
        public LinkedList<Virus> virusCells;

        public LinkedList<EjectedMass> staticEjectedMasses;

        public List<Cell> tmpCellList;

        private int globalPlayerId = 0;
        private int globalCellId = 0;

        public Random random = new Random();

        public NetServer server;

        public double startTime;
        public double time;

        private int tickMain;
        private int tickSpawn;

        private Dictionary<GameMessageTypes, GameMessage> gameMessagePool;

        private List<Contact> contacts;
        private int contactCapacity;
        private int contactCount;

        public int GetNewPlayerId()
        {
            if (globalPlayerId == int.MaxValue) globalPlayerId = 1;
            else globalPlayerId++;

            return globalPlayerId;
        }

        public int GetNewCellId()
        {
            if (globalCellId == int.MaxValue) globalCellId = 1;
            else globalCellId++;

            return globalCellId;
        }

        public Position GetRandomPosition()
        {
            float x = (float)random.NextDouble() * (Config.BorderRight - Config.BorderLeft) + Config.BorderLeft;
            float y = (float)random.NextDouble() * (Config.BorderBottom - Config.BorderTop) + Config.BorderTop;

            return new Position(x, y);
        }

        public Position GetRandomSpawn()
        {
            if (foodCells.Count > 0)
            {
                Cell cell = foodCells.First.Value;
                Position pos = cell.position;

                RemoveCell(cell);

                return pos;
            }

            return GetRandomPosition();      
        }

        public Color GetRandomColor()
        {
            int id = random.Next(colors.Count);
            return colors[id];
        }

        public void AddCell(Cell cell)
        {
            cell.OnAdd();

            int numPlayers = players.Count;
            for (int i = 0; i < numPlayers; ++i)
            {
                Player player = players[i];
                if (cell.CheckVisible(player.viewBox))
                {
                    player.cellAddList.Add(cell);
                }
            }
        }

        public void RemoveCell(Cell cell)
        {
            cell.OnRemove();

            int numPlayers = players.Count;
            for (int i = 0; i < numPlayers; ++i)
            {
                Player player = players[i];
                player.cellRemoveList.Add(cell);
            }
        }

        public void CreatePlayerCell(Player owner, Cell parent, float angle, float mass, float speed)
        {
            Position startPos = parent.position;

            PlayerCell playerCell = new PlayerCell(GetNewCellId(), owner, startPos, mass, this);
            playerCell.autoMoveAngle = angle;
            playerCell.SetMoveEngineData(speed, 15);
            playerCell.UpdateMergeTime(Config.PlayerRecombineTime);

            AddCell(playerCell);
            playerCell.StartAutoMove();
        }

        public void CreateVirusSplitCell(PlayerCell parent)
        {
            Player owner = parent.owner;

            int maxSplits = (int)(Math.Floor(parent.mass / 16.0f) + 0.5f) - 1;
            int numSplits = Config.PlayerMaxCells - owner.cells.Count;

            numSplits = Math.Min(numSplits, maxSplits);

            if (numSplits <= 0) return;

            parent.UpdateMergeTime(Config.PlayerRecombineTime);

            float splitMass = Math.Min(parent.mass / (numSplits + 1), 36.0f);

            int bigSplits = 0;
            float endMass = parent.mass - (numSplits * splitMass);

            if ((endMass > 300) && (numSplits > 0))
            {
                bigSplits++;
                numSplits--;
            }

            if ((endMass > 1200) && (numSplits > 0))
            {
                bigSplits++;
                numSplits--;
            }

            if ((endMass > 3000) && (numSplits > 0))
            {
                bigSplits++;
                numSplits--;
            }

            float angle = 0.0f;

            for (int i = 0; i < numSplits; ++i)
            {
                angle += 6.0f / numSplits;
                CreatePlayerCell(owner, parent, angle, splitMass, 150.0f);
                parent.mass -= splitMass;
            }

            for (int i = 0; i < bigSplits; ++i)
            {
                angle = (float)random.NextDouble() * 6.28f;
                splitMass = parent.mass / 4.0f;
                CreatePlayerCell(owner, parent, angle, splitMass, 20.0f);
                parent.mass -= splitMass;
            }
        }

        public void SplitCell(Player player)
        {
            int numCells = player.cells.Count;

            int numSplitCells = Config.PlayerMaxCells - numCells;
            if (numSplitCells <= 0) return;

            float targetX = player.targetX;
            float targetY = player.targetY;

            for (LinkedListNode<PlayerCell> it = player.cells.First; numCells > 0 && numSplitCells > 0; it = it.Next)
            {
                PlayerCell cell = it.Value;
                if (cell.mass < Config.PlayerMinMassSplit)
                {
                    numCells--;
                    continue;
                }

                float deltaX = targetX - cell.position.x;
                float deltaY = targetY - cell.position.y;

                float angle = 0.0f;
                if (Math.Abs(deltaX) > 1e-5f || Math.Abs(deltaY) > 1e-5f) angle = (float)Math.Atan2(deltaX, deltaY);

                float size = cell.GetSize() / 2.0f;

                Position startPos = cell.position;
                startPos.x += size * (float)Math.Sin(angle);
                startPos.y += size * (float)Math.Cos(angle);

                float splitSpeed = cell.GetSpeed() * 6.0f;
                float newMass = cell.mass / 2.0f;
                cell.mass -= newMass;

                PlayerCell split = new PlayerCell(GetNewCellId(), player, startPos, newMass, this);
                split.autoMoveAngle = angle;
                split.SetMoveEngineData(splitSpeed, 32, 0.85f);
                split.UpdateMergeTime(Config.PlayerRecombineTime);

                AddCell(split);
                split.StartAutoMove();

                numCells--;
                numSplitCells--;
            }
        }

        public void EjectMass(Player player)
        {
            float targetX = player.targetX;
            float targetY = player.targetY;

            for (LinkedListNode<PlayerCell> it = player.cells.First; it != null; it = it.Next)
            {
                PlayerCell cell = it.Value;

                if (cell.mass < Config.PlayerMinMassEject) continue;

                float deltaX = targetX - cell.position.x;
                float deltaY = targetY - cell.position.y;

                float angle = 0.0f;
                if (Math.Abs(deltaX) > 1e-5f || Math.Abs(deltaY) > 1e-5f) angle = (float)Math.Atan2(deltaX, deltaY);

                float size = cell.GetSize() + 5.0f;

                Position startPos = cell.position;
                startPos.x += (size + Config.EjectMass) * (float)Math.Sin(angle);
                startPos.y += (size + Config.EjectMass) * (float)Math.Cos(angle);

                cell.mass -= Config.EjectMassLoss;
                angle += (float)random.NextDouble() * 0.4f - 0.2f;

                EjectedMass ejectedMass = new EjectedMass(GetNewCellId(), startPos, Config.EjectMass, this);
                ejectedMass.autoMoveAngle = angle;
                ejectedMass.SetMoveEngineData(Config.EjectSpeed, 20);
                ejectedMass.color = cell.color;

                AddCell(ejectedMass);
                ejectedMass.StartAutoMove();
            }
        }

        public void ShootVirus(Virus parent)
        {
            Position pos = parent.position;
            
            Virus virus = new Virus(GetNewCellId(), pos, Config.VirusStartMass, this);
            virus.autoMoveAngle = parent.autoMoveAngle;
            virus.SetMoveEngineData(200.0f, 20);

            AddCell(virus);
            virus.StartAutoMove();
        }

        public Virus GetVirusConsumer(Cell cell)
        {
            for (LinkedListNode<Virus> it = virusCells.First; it != null; it = it.Next)
            {
                Virus virus = it.Value;
                if (cell.IsInsideCircle(virus.position, virus.GetSize())) return virus;
            }

            return null;
        }

        public GameMessage GetGameMessage(GameMessageTypes gameMessageType)
        {
            return gameMessagePool[gameMessageType];
        }

        public void Init(int port)
        {
            NetPeerConfiguration config = new NetPeerConfiguration("Ugar.io");
            config.Port = port;

            config.EnableMessageType(NetIncomingMessageType.WarningMessage);
            config.EnableMessageType(NetIncomingMessageType.VerboseDebugMessage);
            config.EnableMessageType(NetIncomingMessageType.ErrorMessage);
            config.EnableMessageType(NetIncomingMessageType.Error);
            config.EnableMessageType(NetIncomingMessageType.DebugMessage);
            config.EnableMessageType(NetIncomingMessageType.ConnectionApproval);

            server = new NetServer(config);
            server.Start();

            gameMessagePool = new Dictionary<GameMessageTypes, GameMessage>();
            gameMessagePool.Add(GameMessageTypes.SetTarget, new SetTargetMessage());
            gameMessagePool.Add(GameMessageTypes.SplitCell, new SplitCellMessage());
            gameMessagePool.Add(GameMessageTypes.EjectMass, new EjectMassMessage());
            gameMessagePool.Add(GameMessageTypes.AddCell, new AddCellMessage());
            gameMessagePool.Add(GameMessageTypes.UpdateCell, new UpdateCellMessage());
            gameMessagePool.Add(GameMessageTypes.HideCell, new HideCellMessage());
            gameMessagePool.Add(GameMessageTypes.DestroyCell, new DestroyCellMessage());
            gameMessagePool.Add(GameMessageTypes.UpdateCamera, new UpdateCameraMessage());
            gameMessagePool.Add(GameMessageTypes.GameLose, new GameLoseMessage());
            gameMessagePool.Add(GameMessageTypes.RespawnPlayer, new RespawnPlayerMessage());

            tmpCellList = new List<Cell>();

            players = new List<Player>();
            disconnectPlayers = new List<Player>();

            contacts = new List<Contact>();
            contactCount = 0;
            contactCapacity = 0;

            cells = new LinkedList<Cell>();

            autoMoveCells = new LinkedList<Cell>();

            playerCells = new LinkedList<PlayerCell>();
            foodCells = new LinkedList<Food>();
            virusCells = new LinkedList<Virus>();

            staticEjectedMasses = new LinkedList<EjectedMass>();

            colors = new List<Color>();
            colors.Add(new Color(235, 75, 0));
            colors.Add(new Color(225, 125, 255));
            colors.Add(new Color(180, 7, 20));
            colors.Add(new Color(80, 170, 240));
            colors.Add(new Color(180, 90, 135));
            colors.Add(new Color(195, 240, 0));
            colors.Add(new Color(150, 18, 255));
            colors.Add(new Color(80, 245, 0));
            colors.Add(new Color(165, 25, 0));
            colors.Add(new Color(80, 145, 0));
            colors.Add(new Color(80, 170, 240));
            colors.Add(new Color(55, 92, 255));

            time = NetTime.Now;
            startTime = time;

            tickMain = 0;
            tickSpawn = 0;
        }

        public Contact AddNewContact()
        {
            if (contactCount < contactCapacity)
            {
                return contacts[contactCount++];
            }
            else
            {
                Contact contact = new Contact();
                contacts.Add(contact);

                contactCount++;
                contactCapacity++;

                return contact;
            }
        }

        public void Update()
        {
            NetIncomingMessage msg;
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        Console.WriteLine(msg.ReadString());
                        break;

                    case NetIncomingMessageType.ConnectionApproval:
                        NetOutgoingMessage message = server.CreateMessage();
                        message.Write(Config.BorderLeft);
                        message.Write(Config.BorderRight);
                        message.Write(Config.BorderTop);
                        message.Write(Config.BorderBottom);
                        message.Write(Config.ServerViewBaseX);
                        message.Write(Config.ServerViewBaseY);
                        msg.SenderConnection.Approve(message);
                        break;

                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus)msg.ReadByte();
                        if (status == NetConnectionStatus.Connected)
                        {
                            NetIncomingMessage connectionMsg = msg.SenderConnection.RemoteHailMessage;
                            string playerName = connectionMsg.ReadString();
                            if (playerName.Length > Config.PlayerMaxNickLength) playerName = playerName.Remove(Config.PlayerMaxNickLength);

                            Player player = SpawnPlayer(msg.SenderConnection, playerName);
                            players.Add(player);

                            msg.SenderConnection.Tag = player;
                        }
                        else if (status == NetConnectionStatus.Disconnected)
                        {
                            Player player = msg.SenderConnection.Tag as Player;
                            if (player != null)
                            {
                                player.connection = null;
                                player.disconnectCountdown = Config.PlayerDisconnectTime * 20;

                                players.Remove(player);
                                disconnectPlayers.Add(player);

                                msg.SenderConnection.Tag = null;
                            }
                        }
                        break;

                    case NetIncomingMessageType.Data:
                        GameMessageTypes messageType = (GameMessageTypes)msg.ReadByte();

                        GameMessage gameMessage = GetGameMessage(messageType);
                        gameMessage.Decode(msg);

                        switch (messageType)
                        {
                            case GameMessageTypes.SetTarget:
                                {
                                    Player player = (Player)msg.SenderConnection.Tag;

                                    SetTargetMessage setTargetMessage = (SetTargetMessage)gameMessage;
                                    player.SetTarget(setTargetMessage.dx, setTargetMessage.dy);
                                }
                                break;

                            case GameMessageTypes.SplitCell:
                                {
                                    Player player = (Player)msg.SenderConnection.Tag;
                                    player.splitCellFlag = true;
                                }
                                break;

                            case GameMessageTypes.EjectMass:
                                {
                                    Player player = (Player)msg.SenderConnection.Tag;
                                    player.ejectMassFlag = true;
                                }
                                break;

                            case GameMessageTypes.RespawnPlayer:
                                {
                                    Player player = (Player)msg.SenderConnection.Tag;

                                    RespawnPlayerMessage respawnPlayerMessage = (RespawnPlayerMessage)gameMessage;
                                    StartPlayer(player, respawnPlayerMessage.name);
                                }
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        break;
                }
            }

            double newTime = NetTime.Now;
            if (newTime - time > 0.05f) //50 ms
            {
                time = newTime;

                UpdateCells();
                UpdateSpawn();

                tickMain++;
                if (tickMain >= 20) //1s
                {
                    tickMain = 0;
                    SlowUpdateCells();
                }

                UpdatePlayers();
            }
        }

        public Player SpawnPlayer(NetConnection connection, string name)
        {
            Player player = new Player(connection, this);
            StartPlayer(player, name);
            return player;
        }

        public void StartPlayer(Player player, string name)
        {
            if (player.isActive) return;
            player.isActive = true;

            Position pos = GetRandomSpawn();
            float mass = Config.PlayerStartMass;
            Color color = GetRandomColor();

            if (staticEjectedMasses.Count > 0)
            {
                int prob = random.Next(100);
                if (prob < Config.EjectSpawnPlayer)
                {
                    EjectedMass ejectedMass = staticEjectedMasses.First.Value;

                    pos = ejectedMass.position;
                    mass = ejectedMass.mass;
                    color = ejectedMass.color;

                    RemoveCell(ejectedMass);
                }
            }

            player.color = color;
            player.name = name;

            PlayerCell playerCell = new PlayerCell(GetNewCellId(), player, pos, mass, this);
            AddCell(playerCell);

            player.targetX = playerCell.position.x;
            player.targetY = playerCell.position.y;
        }

        public void UpdateCells()
        {
            for (LinkedListNode<Cell> it = autoMoveCells.First; it != null; )
            {
                Cell cell = it.Value;
                it = it.Next;

                if (cell.moveEngineTicks > 0) cell.AutoMove();
                else cell.StopAutoMove();
            }

            for (LinkedListNode<PlayerCell> it = playerCells.First; it != null; it = it.Next)
            {
                PlayerCell cell = it.Value;
                Player player = cell.owner;

                cell.Move(player.targetX, player.targetY);
            }


            for (LinkedListNode<PlayerCell> it = playerCells.First; it != null; it = it.Next)
            {
                PlayerCell cell = it.Value;
                //if (cell.isAutoMoving()) continue;

                for (LinkedListNode<PlayerCell> it2 = it.Next; it2 != null; it2 = it2.Next)
                {
                    PlayerCell otherCell = it2.Value;

                    if (otherCell.owner != cell.owner) continue;
                    //if (otherCell.isAutoMoving()) continue;
                    if (cell.recombineTicks == 0 && otherCell.recombineTicks == 0) continue;

                    Contact contact = AddNewContact();
                    if (!contact.Check(cell, otherCell)) contactCount--;
                }
            }

            for (int k = 0; k < NUM_CONTACT_RESOLVE_ITERATIONS; ++k)
            {
                for (int i = 0; i < contactCount; ++i)
                {
                    contacts[i].Solve();
                }
            }

            contactCount = 0;
            for (int i = 0; i < contactCount; ++i)
            {
                contacts[i].Reset();
            }

            for (LinkedListNode<PlayerCell> it = playerCells.First; it != null; it = it.Next)
            {
                PlayerCell cell = it.Value;

                float x = cell.position.x + cell.moveX;
                float y = cell.position.y + cell.moveY;

                float radius = cell.GetSize() / 2.0f;

                if (x - radius < Config.BorderLeft) x = Config.BorderLeft + radius;
                else if (x + radius > Config.BorderRight) x = Config.BorderRight - radius;

                if (y - radius < Config.BorderTop) y = Config.BorderTop + radius;
                else if (y + radius > Config.BorderBottom) y = Config.BorderBottom - radius;

                cell.position.x = x;
                cell.position.y = y;
            }

            for (LinkedListNode<PlayerCell> it = playerCells.First; it != null;)
            {
                PlayerCell cell = it.Value;
                it = it.Next;

                GetEatenCellList(cell, ref tmpCellList);

                int numEatenCells = tmpCellList.Count;
                for (int i = 0; i < numEatenCells; ++i)
                {
                    Cell otherCell = tmpCellList[i];

                    otherCell.OnConsume(cell);

                    otherCell.killer = cell;
                    RemoveCell(otherCell);
                }
            }
        }

        public void SlowUpdateCells()
        {
            float massDecay = 1.0f - Config.PlayerMassDecayRate;
            
            for (LinkedListNode<PlayerCell> it = playerCells.First; it != null; it = it.Next)
            {
                PlayerCell cell = it.Value;

                if (cell.recombineTicks > 0) cell.recombineTicks--;
                if (cell.mass > Config.PlayerMinMassDecay) cell.mass *= massDecay;
            }
        }

        public void UpdateSpawn()
        {
            tickSpawn++;

            if (tickSpawn >= Config.SpawnInterval)
            {
                UpdateSpawnFood();
                UpdateSpawnVirus();

                tickSpawn = 0;
            }
        }

        public void StartSpawnFood()
        {
            for (int i = 0; i < Config.FoodStartAmount; ++i) SpawnFood();
        }
        
        public void UpdateSpawnFood()
        {
            int numSpawnFoods = Math.Min(Config.FoodSpawnAmount, Config.FoodMaxAmount - foodCells.Count);
            for (int i = 0; i < numSpawnFoods; ++i) SpawnFood();
        }

        public void SpawnFood()
        {
            Food food = new Food(GetNewCellId(), GetRandomPosition(), Config.FoodMass, this);
            food.color = GetRandomColor();

            AddCell(food);
        }

        public void UpdateSpawnVirus()
        {
            if (virusCells.Count < Config.VirusMinAmount)
            {
                Position pos = GetRandomPosition();
                float size = (float)Math.Sqrt(Config.VirusStartMass * 100);

                for (LinkedListNode<PlayerCell> it = playerCells.First; it != null; it = it.Next)
                {
                    PlayerCell cell = it.Value;

                    if (cell.mass < Config.VirusStartMass) continue;
                    
                    var squareR = cell.GetSquareSize(); 
            
                    var dx = cell.position.x - pos.x;
                    var dy = cell.position.y - pos.y;

                    if (cell.CoverCircle(pos, size)) return;
                }

                Virus virus = new Virus(GetNewCellId(), pos, Config.VirusStartMass, this);
                AddCell(virus);
            }
        }

        public void GetEatenCellList(PlayerCell cell, ref List<Cell> lst)
        {
            lst.Clear();

            Position center = cell.position;
            float r = cell.GetSize();

            List<Cell> visibleCells = cell.owner.visibleCells;
            int numVisibleCells = visibleCells.Count;
            for (int i = 0; i < numVisibleCells; ++i)
            {
                Cell otherCell = visibleCells[i];

                if (cell.id == otherCell.id) continue;
                if (cell.isAutoMoving() && otherCell.owner == cell.owner) continue;

                if (!otherCell.IsInsideCircle(center, r)) continue;

                float multiplier = 1.25f;
                switch (otherCell.cellType)
                {
                    case CellType.FOOD:
                        lst.Add(otherCell);
                        continue;

                    case CellType.VIRUS:
                        multiplier = 1.33f;
                        break;

                    case CellType.PLAYER_CELL:
                        if (otherCell.owner == cell.owner)
                        {
                            if (cell.recombineTicks > 0 || otherCell.recombineTicks > 0) continue;
                            multiplier = 1.00f;
                        }
                        break;

                    default:
                        break;
                }

                if (otherCell.mass * multiplier > cell.mass + 1e-5f) continue;

                float dist = cell.GetDist(otherCell.position);
                float eatingRange = cell.GetSize() - otherCell.GetEatingRange();
                if (dist > eatingRange + 1e-5f) continue;

                lst.Add(otherCell);
            }
        }

        public void UpdatePlayers()
        {
            int numPlayers = disconnectPlayers.Count;

            int pos = 0;
            for (int i = 0; i < numPlayers; ++i)
            {
                Player player = disconnectPlayers[i];
                if (player.disconnectCountdown > 0)
                {
                    player.disconnectCountdown--;
                    if (player.disconnectCountdown == 0)
                    {
                        for (LinkedListNode<PlayerCell> it = player.cells.First; it != null; )
                        {
                            PlayerCell cell = it.Value;
                            it = it.Next;

                            RemoveCell(cell);
                        }
                    }
                    else
                    {
                        disconnectPlayers[pos++] = player;
                    }
                }
                else
                {
                    disconnectPlayers[pos++] = player;
                }
            }

            if (pos < numPlayers)
            {
                disconnectPlayers.RemoveRange(pos, numPlayers - pos);
            }

            numPlayers = players.Count;
            for (int i = 0; i < numPlayers; ++i)
            {
                Player player = players[i];
                player.Update();
            }
        }

        static void Main(string[] args)
        {
            CommandLineDictionary d = CommandLineDictionary.FromArguments(args, '-', '=');

            //string game_id = d["game_id"];
            //string game_build_version = d["game_build_version"];
            //string game_mode = d["game_mode"];
            //string server_host_domain = d["server_host_domain"];
            string server_host_port = d["server_host_port"];
            //string server_host_region = d["server_host_region"];
            //string playfab_api_endpoint = d["playfab_api_endpoint"];
            //string title_secret_key = d["title_secret_key"];
            //string custom_data = d["custom_data"];
            //string log_file_path = d["log_file_path"];
            //string output_files_directory_path = d["output_files_directory_path"];

            GameServer gameServer = new GameServer();
            gameServer.Init(int.Parse(server_host_port));

            while (!Console.KeyAvailable || Console.ReadKey().Key != ConsoleKey.Escape)
            {
                gameServer.Update();
                Thread.Sleep(1);
            }
        }
    }
}
