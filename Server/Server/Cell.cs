﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Cell
    {
        public int id;

        public Color color;
        
        public Position position;
        public float mass;

        public CellType cellType = CellType.INVALID;

        public Cell killer = null;
        public Player owner = null;

        public int recombineTicks = 0;

        public GameServer gameServer;

        public int moveEngineTicks = 0;
        public float moveEngineSpeed = 0.0f;
        public float moveDecay = 0.75f;

        public float autoMoveAngle = 0.0f;

        public bool forceSendUpdate = false;

        public LinkedListNode<Cell> cellNode = null;
        public LinkedListNode<Cell> autoMoveCellNode = null;

        public Cell(int id, Position position, float mass, GameServer gameServer)
        {
            this.id = id;
            this.position = position;
            this.mass = mass;
            this.gameServer = gameServer;
        }

        public void SetMoveEngineData(float speed, int ticks, float decay = 0.75f)
        {
            moveEngineSpeed = speed;
            moveEngineTicks = ticks;
            moveDecay = decay;
        }

        public string GetName()
        {
            if (owner != null) return owner.name;
            else return string.Empty;
        }

        public virtual float GetSize()
        {
            return (float)Math.Sqrt(100.0f * mass);
        }

        public virtual float GetSquareSize()
        {
            return 100.0f * mass;
        }

        public virtual float GetEatingRange()
        {
            return 0.0f;
        }       

        public float GetDist(float x, float y)       
        {
            return (float)Math.Sqrt((x - position.x) * (x - position.x) + (y - position.y) * (y - position.y));
        }

        public float GetDist(Position pos)
        {
            return (float)Math.Sqrt((pos.x - position.x) * (pos.x - position.x) + (pos.y - position.y) * (pos.y - position.y));
        }
        
        public bool IsInside(Box box)
        {
            return position.x >= box.left && position.x <= box.right &&
                position.y >= box.top && position.y <= box.bottom;
        }

        public bool IsInsideCircle(Position center, float radius)
        {
            float size = GetSize();

            if (size > radius + 1e-5f) return false;

            float dr = radius - size * 0.9f;

            float dx = position.x - center.x;
            float dy = position.y - center.y;

            return dx * dx + dy * dy < dr * dr;
        }

        public bool CoverCircle(Position center, float radius)
        {
            float size = GetSize();

            if (radius > size + 1e-5f) return false;

            float dr = size - radius * 0.9f;

            float dx = position.x - center.x;
            float dy = position.y - center.y;

            return dx * dx + dy * dy < dr * dr;
        }

        public virtual bool ShouldSendUpdate()
        {
            return true;
        }

        public virtual bool CheckVisible(Box box)
        {
            return IsInside(box);
        }

        public virtual void AutoMove()
        {
            float x = position.x + moveEngineSpeed * (float)Math.Sin(autoMoveAngle);
            float y = position.y + moveEngineSpeed * (float)Math.Cos(autoMoveAngle);

            moveEngineSpeed *= moveDecay;
            moveEngineTicks--;

            float radius = GetSize() / 2.0f;
            
            if ((position.x - radius) < Config.BorderLeft) 
            {
                autoMoveAngle = 6.28f - autoMoveAngle;
                x = Config.BorderLeft + radius;
            }

            if ((position.x + radius) > Config.BorderRight) 
            {
                autoMoveAngle = 6.28f - autoMoveAngle;
                x = Config.BorderRight - radius;
            }

            if ((position.y - radius) < Config.BorderTop) 
            {
                autoMoveAngle = (autoMoveAngle <= 3.14f) ? 3.14f - autoMoveAngle : 9.42f - autoMoveAngle;
                y = Config.BorderTop + radius;
            }

            if ((position.y + radius) > Config.BorderBottom)
            {
                autoMoveAngle = (autoMoveAngle <= 3.14f) ? 3.14f - autoMoveAngle : 9.42f - autoMoveAngle;
                y = Config.BorderBottom - radius;
            }

            position.x = x;
            position.y = y;
        }
        
        public virtual void OnRemove()
        {
            if (cellNode != null)
            {
                gameServer.cells.Remove(cellNode);
                cellNode = null;
            }

            if (autoMoveCellNode != null)
            {
                gameServer.autoMoveCells.Remove(autoMoveCellNode);
                autoMoveCellNode = null;
            }
        }

        public virtual void OnAdd()
        {
           cellNode = gameServer.cells.AddLast(this);
        }

        public virtual void OnConsume(PlayerCell consumer)
        {
            consumer.AddMass(mass);
        }

        public bool isAutoMoving()
        {
            return autoMoveCellNode != null;
        }

        public virtual void StartAutoMove()
        {
            if (autoMoveCellNode == null)
            {
                autoMoveCellNode = gameServer.autoMoveCells.AddLast(this);
            }
        }

        public virtual void StopAutoMove()
        {
            moveEngineTicks = 0;

            if (autoMoveCellNode != null)
            {
                gameServer.autoMoveCells.Remove(autoMoveCellNode);
                autoMoveCellNode = null;
            }
        }
    }
}
