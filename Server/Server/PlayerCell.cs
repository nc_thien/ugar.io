﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class PlayerCell : Cell
    {
        public LinkedListNode<PlayerCell> playerCellNode = null;
        public LinkedListNode<PlayerCell> ownerCellNode = null;

        public float moveX = 0.0f;
        public float moveY = 0.0f;
        
        public PlayerCell(int id, Player owner, Position position, float mass, GameServer gameServer) : base(id, position, mass, gameServer)
        {
            this.owner = owner;
            cellType = CellType.PLAYER_CELL;
        }

        public void AddMass(float value)
        {
            if (mass + value > Config.PlayerMaxMass && owner.cells.Count < Config.PlayerMaxCells)
            {
                mass = (mass + value) / 2.0f;
                gameServer.CreatePlayerCell(owner, this, 0.0f, mass, 150);
            }
            else
            {
                mass = Math.Min(mass + value, Config.PlayerMaxMass);
            }
        }

        public float GetSpeed()
        {
            // Based on 50ms tick
            return (float)(30 * Math.Pow(mass, -1.0 / 4.5) * 50 / 40);
        }

        public override float GetEatingRange()
        {
            return GetSize() * 0.4f;
        }       

        public override bool CheckVisible(Box box)
        {
            if (mass < 150.0f) return IsInside(box);
            else
            {
                float size = GetSize();
                float xRange = size + box.extents.w;
                float yRange = size + box.extents.h;

                return Math.Abs(position.x - box.center.x) < xRange && Math.Abs(position.y - box.center.y) < yRange;
            }
        }

        public void UpdateMergeTime(int baseTick)
        {
            recombineTicks = baseTick + (int)(0.02f * mass);
        }

        public void Move(float x, float y)
        {
            moveX = x - position.x;
            moveY = y - position.y;

            float dist = (float)Math.Sqrt(moveX * moveX + moveY * moveY);
            float speed = GetSpeed();

            if (dist > speed)
            {
                float ratio = speed / dist;
                moveX *= ratio;
                moveY *= ratio;
            }
        }

        public override void OnRemove()
        {
            base.OnRemove();

            if (playerCellNode != null)
            {
                gameServer.playerCells.Remove(playerCellNode);
                playerCellNode = null;
            }

            if (ownerCellNode != null)
            {
                owner.cells.Remove(ownerCellNode);
                ownerCellNode = null;
            }
        }

        public override void OnAdd()
        {
            base.OnAdd();

            playerCellNode = gameServer.playerCells.AddLast(this);
            if (owner != null)
            {
                color = owner.color;
                ownerCellNode = owner.cells.AddLast(this);
            }
        }
    }
}
