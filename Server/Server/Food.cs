﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Food : Cell
    {
        private float size;
        private float squareSize;

        public LinkedListNode<Food> foodCellNode = null;

        public Food(int id, Position position, float mass, GameServer gameServer) : base(id, position, mass, gameServer)
        {
            cellType = CellType.FOOD;

            size = base.GetSize();
            squareSize = base.GetSquareSize();
        }

        public override float GetSize()
        {
            return size;
        }

        public override float GetSquareSize()
        {
            return squareSize;
        }

        public override bool ShouldSendUpdate()
        {
            return moveEngineTicks > 0 || forceSendUpdate;
        }

        public override void StopAutoMove()
        {
            base.StopAutoMove();
            forceSendUpdate = true;
        }

        public override void OnRemove()
        {
            base.OnRemove();

            if (foodCellNode != null)
            {
                gameServer.foodCells.Remove(foodCellNode);
                foodCellNode = null;
            }
        }

        public override void OnAdd()
        {
            base.OnAdd();
            foodCellNode = gameServer.foodCells.AddLast(this);
        }
    }
}
