﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Contact
    {
        public PlayerCell a;
        public PlayerCell b;

        public float impulse;

        public float nx;
        public float ny;

        public void Reset()
        {
            a = null;
            b = null;
        }

        public bool Check(PlayerCell a, PlayerCell b)
        {
            float sizea = a.GetSize();
            float sizeb = b.GetSize();

            float minxa = a.position.x + Math.Min(a.moveX, 0.0f) - sizea;
            float maxxa = a.position.x + Math.Max(a.moveX, 0.0f) + sizea;

            float minya = a.position.y + Math.Min(a.moveY, 0.0f) - sizea;
            float maxya = a.position.y + Math.Max(a.moveY, 0.0f) + sizea;

            float minxb = b.position.x + Math.Min(b.moveX, 0.0f) - sizea;
            if (minxb > maxxa) return false;

            float maxxb = b.position.x + Math.Max(b.moveX, 0.0f) + sizea;
            if (maxxb < minxa) return false;

            float minyb = b.position.y + Math.Min(b.moveY, 0.0f) - sizea;
            if (minyb > maxya) return false;

            float maxyb = b.position.y + Math.Max(b.moveY, 0.0f) + sizea;
            if (maxyb < minya) return false;

            float dx = b.position.x - a.position.x;
            float dy = b.position.y - a.position.y;

            float dist = (float)Math.Sqrt(dx * dx + dy * dy);
            nx = ny = 0.0f;

            if (dist < 1e-5f)
            {
                if (dx >= 0.0f) nx = 1.0f;
                else nx = -1.0f;
            }
            else
            {
                nx = dx / dist;
                ny = dy / dist;
            }

            this.a = a;
            this.b = b;

            impulse = 0.0f;
            return true;
        }

        public void Solve()
        {
            float dx = b.position.x - a.position.x;
            float dy = b.position.y - a.position.y;

            float distSqr = dx * dx + dy * dy;
            float dist = (float)Math.Sqrt(distSqr);

            float sizeSum = a.GetSize() + b.GetSize();

            float penetration = sizeSum - dist;

            float remove = 0.0f;

            float vx = b.moveX - a.moveX;
            float vy = b.moveY - a.moveY;

            if (penetration > 0.0f)
            {
                remove = (vx * nx + vy * ny) - penetration;
            }
            else
            {
                float t = 0.0f;

                float A = vx * vx + vy * vy;
                float B = dx * vx + dy * vy;
                float C = distSqr - sizeSum * sizeSum;

                if (Math.Abs(A) < 1e-5f)
                {
                    if (Math.Abs(B) < 1e-5f) return;
                    t = -C / (B * 2.0f);
                }
                else
                {
                    float delta = B * B - A * C;
                    if (delta < 0.0f) return;

                    delta = (float)Math.Sqrt(delta);

                    float t0 = (-B - delta) / A;
                    float t1 = (-B + delta) / A;

                    if (t0 >= 1.0f || t1 < 0.0f) return;
                    t = (t0 < 0.0f ? t1 : t0);
                }

                remove = (vx * nx + vy * ny) * (1.0f - t);
            }

            float newImpulse = Math.Min(remove / (1.0f / a.mass + 1.0f / b.mass) + impulse, 0.0f);

            float change = newImpulse - impulse;
            impulse = newImpulse;

            float val = change / a.mass;
            a.moveX += val * nx;
            a.moveY += val * ny;

            val = change / b.mass;
            b.moveX -= val * nx;
            b.moveY -= val * ny;
        }
    }
}
