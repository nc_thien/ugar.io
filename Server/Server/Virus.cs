﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public class Virus : Cell
    {
        private int fed = 0;

        public LinkedListNode<Virus> virusCellNode = null;

        public Virus(int id, Position position, float mass, GameServer gameServer)
            : base(id, position, mass, gameServer)
        {
            cellType = CellType.VIRUS;
            color = new Color(0, 255, 0);
        }

        public override bool ShouldSendUpdate()
        {
            return moveEngineTicks > 0 || forceSendUpdate;
        }

        public override void StopAutoMove()
        {
            base.StopAutoMove();
            forceSendUpdate = true;
        }

        public void Feed(EjectedMass feeder)
        {
            autoMoveAngle = feeder.autoMoveAngle;
            mass += feeder.mass;
            fed++;

            if (fed >= Config.VirusFeedAmount)
            {
                mass = Config.VirusStartMass;
                fed = 0;

                gameServer.ShootVirus(this);
            }
        }

        public override float GetEatingRange()
        {
            return GetSize() * 0.4f;
        }       

        public override void OnConsume(PlayerCell consumer)
        {
            base.OnConsume(consumer);
            gameServer.CreateVirusSplitCell(consumer);
        }

        public override void OnRemove()
        {
            base.OnRemove();

            if (virusCellNode != null)
            {
                gameServer.virusCells.Remove(virusCellNode);
                virusCellNode = null;
            }
        }

        public override void OnAdd()
        {
            base.OnAdd();
            virusCellNode = gameServer.virusCells.AddLast(this);
        }
    }
}
