﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server
{
    public static class Config
    {
        public const int ServerMaxConnections = 64;  
        public const int ServerViewBaseX = 1024;
        public const int ServerViewBaseY = 576;

        public const float BorderLeft = 0.0f;
        public const float BorderRight = 6000.0f;
        public const float BorderTop = 0.0f;
        public const float BorderBottom = 6000.0f;

        public const int SpawnInterval = 20;                //tick (1 tick = 50 ms)
        public const int FoodSpawnAmount = 10;
        public const int FoodStartAmount = 100;
        public const int FoodMaxAmount = 500;
        public const float FoodMass = 1.0f;

        public const int VirusMinAmount = 10;
        public const int VirusMaxAmount = 50;
        public const float VirusStartMass = 100.0f;
        public const float VirusFeedAmount = 7.0f;
        
        public const float EjectMass = 12.0f;
        public const float EjectMassLoss = 16.0f;
        public const float EjectSpeed = 160.0f;
        public const int EjectSpawnPlayer = 50;             //chance player spawn from ejected mass
        
        public const float PlayerStartMass = 10.0f;
        public const float PlayerMaxMass = 22500.0f;
        public const float PlayerMinMassEject = 32.0f;
        public const float PlayerMinMassSplit = 36.0f;
        public const int PlayerMaxCells = 16;
        public const int PlayerRecombineTime = 30;          //second
        public const float PlayerMassDecayRate = 0.002f;    //mass per second
        public const float PlayerMinMassDecay = 9.0f;
        public const int PlayerMaxNickLength = 15;
        public const int PlayerDisconnectTime = 1;         //second
    }
}
